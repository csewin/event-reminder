import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailTest {

    public static void main(String[] args) throws MessagingException {
        final String username = "testjavaemailclass@gmail.com ";
        final String password = "java12345";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        System.out.println("Salut");

        MimeMessage message = new MimeMessage(session);
        message.setText("Salut, ce mai faci?");
        message.setSubject("Salut");
        message.setRecipient(Message.RecipientType.TO,new InternetAddress("trance1st@yahoo.com"));
        while (true) {
            Transport.send(message);
        }




    }
}
